%%%-------------------------------------------------------------------
%%% @author jesper svennebring <jesper.svennebring@gmail.com>
%%% @copyright (C) 2013, jesper svennebringls
%%% @doc
%%%
%%% A gen_server lobby for handeling game lobbys.
%%%
%%% @end
%%% Created :  6 May 2013 by jesper svennebring <jesper.svennebring@gmail.com>
%%%-------------------------------------------------------------------
-module(lobby).

-behaviour(gen_server).

%% API
-export([start_link/0]).
-export([join_game_lobby/2]).
-export([game_list/1]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
	 terminate/2, code_change/3]).

-define(SERVER, ?MODULE). 

-record(state, {game_list :: [{Name :: atom(), Pid :: pid(), Players :: integer()}],
                game_lobby_sup
               }).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid}
%% @end
%%--------------------------------------------------------------------
start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%--------------------------------------------------------------------
%% @doc
%% Sends a request to the lobby (with Pid) for the pid of the
%% game_lobby serving Game. {ok, LobbyPid} when there are no problems,
%% otherwise {error, Reason} where reason may be no_game or
%% no_admittance. Game is an atom.
%%
%% @spec join_game_lobby(Pid, Game) -> {ok, LobbyPid} |
%%                                     {error, Rrason}
%% @end
%%--------------------------------------------------------------------
join_game_lobby(Pid, Game) ->
    gen_server:call(Pid,{join_game_lobby, Game}).

%%--------------------------------------------------------------------
%% @doc
%% Gets the list with the name of all availible game modules and
%% and the ammount of players in corresponding game_lobby.
%%
%% @spec game_list(Pid) -> GameList
%% @end
%%--------------------------------------------------------------------
game_list(Pid) ->
    gen_server:call(Pid, game_list).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================


%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%% 
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
    {ok, SupPid} = game_lobby_sup:start_link(get_game_list()),
    GameList = try_get_game_list(SupPid),
    erlang:send_after(1000, self(), game_list_update),
    {ok, #state{game_list = add_zero(GameList),
		game_lobby_sup = SupPid}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% _From -> Pid och något mer
%% State -> serverns nuvarande state
%% Reply -> skickas till clienten
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call(game_list, _From, State) ->
    Reply = remove_pid(State#state.game_list) ,
    {reply, Reply, State};
handle_call({join_game_lobby, Game}, _From, State) ->
    Reply = case get_game_pid(Game ,remove_players(State#state.game_list)) of
		error ->
		    {error, no_game};
		Pid ->
		    {ok, Pid}
	    end,
    {reply, Reply, State};
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast(_Msg, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info(game_list_update, State) ->
    {noreply, game_list_update(State)};
    
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================


%%--------------------------------------------------------------------
%% @private
%% @doc
%% Gets the list with the name of all availible game modules as atoms.
%%
%% @spec get_game_list() -> ModuleList
%% @end
%%--------------------------------------------------------------------
get_game_list() ->
    {ok, Games} = application:get_env(games),
    Games.
    
%%--------------------------------------------------------------------
%% @private
%% @doc
%% Makes a tuple list with all the game moduls paired with the
%% respective amount of players in them.
%%
%% @spec game_list_pairing(Games, SupPid) -> [{Game, Pid, Players}]
%% @end
%%--------------------------------------------------------------------
game_list_pairing(Games, SupPid) ->
    try
	lists:map(fun({Module, Pid, _Num}) ->
			  {_Name, Num} = game_lobby:gamelist(Pid),
			  {Module, Pid, Num}
		  end,
		  Games)
    catch
	exit:{noproc, {gen_server, call, _}} ->
	    GameList = try_get_game_list(SupPid),        
	    game_list_pairing(add_zero(GameList), SupPid)
    end.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Gets the list of games from game lobby sup, and if a game lobby is
%% restarting and can't be reatched, it will retry in a short while.
%%
%% @spec try_get_game_list(Pid) -> [{Game, Pid}]
%% @end
%%--------------------------------------------------------------------
try_get_game_list(SupPid) ->
    case game_lobby_sup:game_list(SupPid) of
        {ok, List} -> List;
        {error, restarting} ->
            timer:sleep(10),
            try_get_game_list(SupPid) 
    end.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Checks if given string is in the tupple list, it it is then it returns
%% its matching int, else returns error. Game is an atom.
%%
%% @spec get_game_pid(Game, GameList) -> Pid | error
%% @end
%%--------------------------------------------------------------------
get_game_pid(Game, List) ->
    proplists:get_value(Game, List, error).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Updates the state game_list to keep it up to date once every sec.
%% 
%%
%% @spec game_list_update(State) -> state
%% @end
%%--------------------------------------------------------------------
game_list_update(State) ->
    GameList = State#state.game_list, 
    SupPid = State#state.game_lobby_sup,
    GameList2 = game_list_pairing(GameList, SupPid),
    erlang:send_after(1000, self(), game_list_update),
    State#state{game_list = GameList2}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Makes a new tupple list out of an old one, with the first and the 
%% last elements of the tupple in.
%%
%% @spec remove_pid([{Game, Pid, Players}]) -> [{Game, Players}]
%% @end
%%--------------------------------------------------------------------
remove_pid(List) ->
    lists:map(fun({Name, _, Players}) ->
		      {Name, Players}
	      end,
	      List).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Makes a new tupple list out of an old one, with the first and the 
%% second elements of the tupple in.
%%
%% @spec remove_players([{Game, Pid, Players}]) -> [{Game, Pid}]
%% @end
%%--------------------------------------------------------------------
remove_players(List) ->
    lists:map(fun({Name, Pid, _}) ->
		      {Name, Pid}
	      end,
	      List).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Makes a 2-tuple list into a 3-tuple list where the last cell will be 0.
%%
%% @spec add_zero([{Game, Pid}]) -> [{Game, Pid, 0}]
%% @end
%%--------------------------------------------------------------------
add_zero(List) ->
    lists:map(fun({Game, Pid}) ->
		      {Game, Pid, 0}
	      end,
	      List).
