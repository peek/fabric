%%%-------------------------------------------------------------------
%%% @author Per Ekemark <per@EKEMPHV4>
%%% @copyright (C) 2013, Per Ekemark
%%% @doc
%%% The guessing game where two players can guess a number and see
%%% who was closest without guessing over.
%%%
%%% @end
%%% Created : 20 May 2013 by Per Ekemark <per@EKEMPHV4>
%%%-------------------------------------------------------------------
-module(guessing_game).

-behaviour(gen_server).
-behaviour(game).

%% API

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

%% game callbacks
-export([start_link/1]).
-export([start_game/1]).
-export([connect/2]).
-export([disconnect/2]).
-export([connected/1]).
-export([receive_msg/2]).
-export([disp_name/0]).
-export([rules/0]).
-export([players/0]).

-define(SERVER, ?MODULE).
-define(SLEEP, 10000).
-define(IN_RANGE(X, A, B), (A =< X andalso X =< B)).

-record(player, {pid :: pid(),
                 ref :: reference(),
                 guess = -1 :: integer()
                }).

-record(state, {players :: [#player{}],
                target :: integer()
               }).

%%%===================================================================
%%% API
%%%===================================================================

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init(Players) ->
    Players2 = lists:map(fun(Pid) ->
                                 #player{pid = Pid,
                                         ref = monitor(process, Pid)}
                         end,
                         Players),
    random:seed(now()),
    Target = random:uniform(100),
    {ok, #state{players = Players2, target = Target}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call(connected, _From, State) ->
    Reply = lists:map(fun(#player{pid = Pid}) -> Pid end, State#state.players),
    {reply, Reply, State};
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast({msg, PlayerPid, Data}, State) ->
    try list_to_integer(binary_to_list(Data)) of
        Number when ?IN_RANGE(Number, 1, 100) ->
            Players = update_guess(Number, PlayerPid, State#state.players),
            send_one("You guessed " ++ integer_to_list(Number) ++ ".",
                     PlayerPid),
            send_all_except("Opponent made a guess", Players, PlayerPid),
            case all_guessed(Players) of
                true ->
                    {Best, {W, L}} = results(State#state.target, Players),
                    Stat = "Target: " ++
                        integer_to_list(State#state.target) ++
                        " Best: " ++
                        case ?IN_RANGE(Best, 1, 100) of
                            true ->
                                integer_to_list(Best);
                            _ ->
                                "Everyone over"
                        end,
                    case length(W) =< 1 of
                        true ->
                            send_all("Winner! " ++ Stat, W);
                        _ ->
                            send_all("Winner! (Draw) " ++ Stat, W)
                    end,
                    send_all("Looser! " ++ Stat, L),
                    timer:sleep(?SLEEP),
                    {stop, normal, State#state{players = Players}};
                false ->
                    {noreply, State#state{players = Players}}
            end;
        _ ->
            send_one("You need to guess between 1 and 100!", PlayerPid),
            {noreply, State}
    catch
        error:badarg ->
            send_one("Please guess on a number between 1 and 100.", PlayerPid),
            {noreply, State}
    end;
handle_cast(game_start, State) ->
    send_all("The game has started, please guess a number.",
             State#state.players),
    {noreply, State};
handle_cast({disconnect, PlayerPid}, State) ->
    Players = remove_player(PlayerPid, State#state.players),
    send_all("Opponent yielded, you winz on walkover!", Players),
    timer:sleep(?SLEEP),
    {stop, normal, State#state{players = Players}};
handle_cast(_Msg, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info({'DOWN', _Ref, process, Pid, _Reason}, State) ->
    Players = remove_player(Pid, State#state.players),
    send_all("Opponent dissappeared, guess you win.", Players),
    timer:sleep(?SLEEP),
    {stop, normal, State#state{players = Players}};
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, State) ->
    lists:foreach(fun(#player{ref = Ref}) ->
                          demonitor(Ref, [flush])
                  end,
                  State#state.players),
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% game callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the game with Players as the initially connected players.
%% @end
%%--------------------------------------------------------------------
-spec start_link(Players :: [pid()]) ->
                        {ok, Pid :: pid()} |
                        ignore |
                        {error, Error :: term()}.
start_link(Players) ->
    gen_server:start_link(?MODULE, Players, []).

%%--------------------------------------------------------------------
%% @doc
%% Messages the game with GamePit that the players are ready.
%% @end
%%--------------------------------------------------------------------
-spec start_game(GamePid :: pid()) -> ok.
start_game(GamePid) ->
    gen_server:cast(GamePid, game_start),
    ok.

%%--------------------------------------------------------------------
%% @doc
%% Players cannot connect to guessing_game, therefore this
%% function always returns error.
%% @end
%%--------------------------------------------------------------------
-spec connect(PlayerPid :: pid(), GamePid :: pid()) ->
                        ok |
                        error.
connect(_PlayerPid, _GamePid) ->
    error. %% Players can't connect to this game after start.

%%--------------------------------------------------------------------
%% @doc
%% Disconnects the player with PlayerPid from the game with GamePid. 
%% @end
%%--------------------------------------------------------------------
-spec disconnect(PlayerPid :: pid(), GamePid :: pid()) ->
                        ok.
disconnect(PlayerPid, GamePid) ->
    gen_server:cast(GamePid, {disconnect, PlayerPid}),
    ok.

%%--------------------------------------------------------------------
%% @doc
%% Returns a list of the players connected to the game with GamePid.
%% @end
%%--------------------------------------------------------------------
-spec connected(GamePid :: pid()) ->
                       Players :: [pid()].
connected(GamePid) ->
    gen_server:call(GamePid, connected).

%%--------------------------------------------------------------------
%% @doc
%% Sends a message to the game with GamePid.
%% @end
%%--------------------------------------------------------------------
-spec receive_msg(GamePid :: pid(),
                  Data :: jsx:json_term() |
                          binary() |
                          number()) ->
                         ok.
receive_msg(GamePid, Data) ->
    gen_server:cast(GamePid, {msg, self(), Data}),
    ok.

%%--------------------------------------------------------------------
%% @doc
%% Returns the display name of the game.
%% @end
%%--------------------------------------------------------------------
-spec disp_name() -> Name :: string().
disp_name() ->
    "Guessing Game".

%%--------------------------------------------------------------------
%% @doc
%% Returns the rules of the game.
%% @end
%%--------------------------------------------------------------------
-spec rules() -> Rules :: string().
rules() ->
    "Each player guesses a number. The goal is to be closest to the target without going over. The target is between 1 and 100.".

%%--------------------------------------------------------------------
%% @doc
%% Returns the minimum (Min) and maximum (Max) number of players
%% the game requires.
%% @end
%%--------------------------------------------------------------------
-spec players() -> {Min :: pos_integer(), Max :: pos_integer()}.
players() ->
    {2, 2}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Removes the player with Pid from the record list Players, order is
%% preserved.
%% @end
%%--------------------------------------------------------------------
-spec remove_player(Pid :: pid(), Players0 :: [#player{}]) ->
                           Players1 :: [#player{}].
remove_player(Pid, Players) ->
    lists:reverse(lists:foldl(fun(Player, Acc) ->
                                      case Player#player.pid =:= Pid of
                                          true ->
                                              Acc;
                                          _ ->
                                              [Player|Acc]
                                      end
                              end,
                              [],
                              Players)).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Sends Msg to all players in Players.
%% @end
%%--------------------------------------------------------------------
-spec send_all(Msg :: string(), Players :: [#player{}]) -> ok.
send_all(Msg, Players) ->
    lists:foreach(fun(#player{pid = Pid}) ->
                          game:send_msg(Pid, list_to_binary(Msg))
                  end,
                  Players).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Sends Msg to Player.
%% @end
%%--------------------------------------------------------------------
-spec send_one(Msg :: string(), Player :: pid() | #player{}) -> ok.
send_one(Msg, Player) ->
    Pid = if is_pid(Player) ->
                  Player;
             is_record(Player, player) ->
                  Player#player.pid
          end,
    game:send_msg(Pid, list_to_binary(Msg)),
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Sends Msg to all players in Players except Player.
%% @end
%%--------------------------------------------------------------------
-spec send_all_except(Msg :: string(),
                     Players :: [#player{}],
                     Player :: pid() | #player{}) ->
                            ok.
send_all_except(Msg, Players, Player) ->
    Except = if is_pid(Player) ->
                     Player;
                is_record(Player, player) ->
                     Player#player.pid
             end,
    lists:foreach(fun(#player{pid = Pid}) ->
                          case Except =:= Pid of
                              true ->
                                  ok;
                              _ ->
                                  game:send_msg(Pid, list_to_binary(Msg))
                          end
                  end,
                  Players).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Update the guess field with Guess in the record from Players0
%% containing Pid.
%% @end
%%--------------------------------------------------------------------
-spec update_guess(Guess :: integer(),
                   Pid :: pid(),
                   Players0 :: [#player{}]) ->
                          Players1 :: [#player{}].
update_guess(Guess, Pid, Players) ->
    lists:map(fun(Player) ->
                      case Player#player.pid =:= Pid of
                          true -> Player#player{guess = Guess};
                          _ -> Player
                      end
              end,
              Players).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Returns true if every player in Players have made a valid guess,
%% otherwise false.
%% @end
%%--------------------------------------------------------------------
-spec all_guessed(Players :: [#player{}]) -> true | false.
all_guessed(Players) ->
    lists:foldl(fun(#player{guess = Guess}, Acc) ->
                        ?IN_RANGE(Guess, 1, 100) andalso Acc
                end,
                true,
                Players).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Splits the list of players into a list of Winners and Losers,
%% the winning guess is returned as Best (0 if all lost).
%% Order is not guaranteed.
%% @end
%%--------------------------------------------------------------------
-spec results(Target :: integer(), Players :: #player{}) ->
                     {Best :: integer(),
                      {Winners :: [#player{}],
                       Losers :: [#player{}]}}.
results(Target, Players) ->
    {BestGuess, {Winners, Losers}} =
        lists:foldl(fun(Player, {Best, {W, L}}) ->
                            Guess = Player#player.guess,
                            if (Guess > Target) orelse (Guess < Best) ->
                                    {Best, {W, sets:add_element(Player, L)}};
                               Guess =:= Best ->
                                    {Best, {sets:add_element(Player, W), L}};
                               true ->
                                    {Guess, {sets:from_list([Player]), sets:union(W, L)}}
                            end
                    end,
                    {0, {sets:new(), sets:new()}},
                    Players),
    {BestGuess, {sets:to_list(Winners), sets:to_list(Losers)}}.
