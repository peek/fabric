%%%-------------------------------------------------------------------
%%% @author Per Ekemark <per@EKEMPHV4>
%%% @copyright (C) 2013, Per Ekemark
%%% @doc
%%% A module and behaviour to interface with a game.
%%% 
%%% @end
%%% Created :  16 May 2013 by Per Ekemark <per@EKEMPHV4>
%%%-------------------------------------------------------------------
-module(game).

%% Callbacks
-export([start_link/1]).
-export([start_game/1]).
-export([connect/2]).
-export([disconnect/2]).
-export([connected/1]).
-export([receive_msg/2]).
-export([disp_name/0]).
-export([rules/0]).
-export([players/0]).

%% API
-export([send_msg/2]).
-export([kick/1]).

%%%===================================================================
%%% Callbacks
%%%===================================================================

-callback start_link(Players :: [pid()]) ->
    {ok, Pid :: pid()} |
    ignore |
    {error, Reason :: term()}.
-callback start_game(GamePid :: pid()) ->
    ok.
-callback connect(PlayerPid :: pid(), GamePid :: pid()) ->
    ok | error.
-callback disconnect(PlayerPid :: pid(), GamePid :: pid()) ->
    ok.
-callback connected(GamePid :: pid()) ->
    Players :: [pid()].
-callback receive_msg(GamePid :: pid(),
                      Data :: jsx:json_term() |
                              binary() |
                              number()) ->
    ok.
-callback disp_name() ->
    Name :: string().
-callback rules() ->
    Rules :: string().
-callback players() ->
    {Min :: pos_integer(), Max :: pos_integer()}.

%% Following functions exist only to allow documentation of the
%% callbacks, they should never be called.

%%--------------------------------------------------------------------
%% @doc
%% Callback to start the game process as a linked process.
%%
%% The Players argument is a list of pids of the initially
%% connected players.
%% Pid is the pid of the newly created process.
%% Reason is the reason for terminating.
%% @end
%%--------------------------------------------------------------------
-spec start_link(Players :: [pid()]) ->
    {ok, Pid :: pid()} |
    ignore |
    {error, Reason :: term()}.
start_link(_) ->
    {error, dummy_function}.

%%--------------------------------------------------------------------
%% @doc
%% Callback to start the game with GamePid.
%%
%% Called when the players are ready to start playing. No message
%% passing to the players may occur prior to this call.
%% @end
%%--------------------------------------------------------------------
-spec start_game(GamePid :: pid()) ->
    ok.
start_game(_) ->
    ok.

%%--------------------------------------------------------------------
%% @doc
%% Callback to connect another player (with PlayerPid to game with
%% GamePid) during play.
%%
%% Returns ok if the player is added to the game or error if
%% the player cannot be connected.
%%
%% @end
%%--------------------------------------------------------------------
-spec connect(PlayerPid :: pid(), GamePid :: pid()) ->
    ok | error.
connect(_, _) ->
    error.

%%--------------------------------------------------------------------
%% @doc
%% Callback to disconnect a player (with PlayerPid from game with
%% GamePid) during play.
%%
%% Returns ok when the player is diconnected.
%% @end
%%--------------------------------------------------------------------
-spec disconnect(PlayerPid :: pid(), GamePid :: pid()) ->
    ok.
disconnect(_, _) ->
    ok.

%%--------------------------------------------------------------------
%% @doc
%% Callback to check which players that are connected to the game
%% with GamePid.
%%
%% Returns a list of the connected players pids.
%% @end
%%--------------------------------------------------------------------
-spec connected(GamePid :: pid()) ->
    Players :: [pid()].
connected(_) ->
    [].

%%--------------------------------------------------------------------
%% @doc
%% Callback to make game with GamePid to receive a message from a
%% player.
%%
%% The Data can have several forms.
%% Returns ok when the message has been received or error
%% if it could not.
%% @end
%%--------------------------------------------------------------------
-spec receive_msg(GamePid :: pid(),
                  Data :: jsx:json_term() |
                          binary() |
                          number()) ->
    ok.
receive_msg(_, _) ->
    ok.

%%--------------------------------------------------------------------
%% @doc
%% Callback to find out the display name for the game.
%%
%% Returns Name, the display name.
%% @end
%%--------------------------------------------------------------------
-spec disp_name() ->
    Name :: string().
disp_name() ->
    "Game".

%%--------------------------------------------------------------------
%% @doc
%% Callback to find out the rules of the game.
%%
%% Returns Rules, a string containing the rules of the game.
%% @end
%%--------------------------------------------------------------------
-spec rules() ->
    Rules :: string().
rules() ->
    "Rules: Do NOT call THIS function!".

%%--------------------------------------------------------------------
%% @doc
%% Callback to find out how many players the game can operate with.
%%
%% Returns the tuple {Min, Max} where Min is the minimum number of
%% players and Max is the maximum number of players. `0 < Min =< Max'
%% @end
%%--------------------------------------------------------------------
-spec players() ->
    {Min :: pos_integer(), Max :: pos_integer()}.
players() ->
    {42, 4711}.

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Sends a message to the player with PlayerPid. Data may be a Json
%% object (not encoded), a binary utf8 encoded string or a number.
%% @end
%%--------------------------------------------------------------------
-spec send_msg(PlayerPid :: pid(),
               Data :: jsx:json_term() |
                       binary() |
                       number()) ->
                      ok |
                      {error, Reason :: term()}.
send_msg(PlayerPid, Data) ->
    user:game(PlayerPid, Data).

%%--------------------------------------------------------------------
%% @doc
%% Notifies the player with PlayerPid that it can no longer take part
%% in the game.
%% @end
%%--------------------------------------------------------------------
-spec kick(PlayerPid :: pid()) -> ok.
kick(PlayerPid) ->
    user:game_stop(PlayerPid).

%% %%--------------------------------------------------------------------
%% %% @doc
%% %% Starts a chat process referenced by ChatPid.
%% %% @end
%% %%--------------------------------------------------------------------
%% -spec chat_start() -> ChatPid :: pid().
%% chat_start() ->
%%     %% start a chat supervisor and process.
%%     %% inform client that the chat has been started
%%     tbi.

%% %%--------------------------------------------------------------------
%% %% @doc
%% %% Stops the chat process referenced by ChatPid.
%% %% @end
%% %%--------------------------------------------------------------------
%% -spec chat_stop(ChatPid :: pid()) -> ok.
%% chat_stop(ChatPid) ->
%%     %% inform client that the chat has been stopped
%%     %% stop the chat supervisor and process.
%%     tbi.

%% %%--------------------------------------------------------------------
%% %% @doc
%% %% Manually invokes an update of connected players in the chat.
%% %% @end
%% %%--------------------------------------------------------------------
%% -spec chat_refresh(ChatPid :: pid()) -> ok.
%% chat_refresh(ChatPid) ->
%%     %% start a chat supervisor and process.
%%     %% inform client that the chat has been started
%%     tbi.
