
-module(fabric_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).
-export([lobby_pid/1]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).
-define(LOBBY, lobby).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

lobby_pid(SupPid) ->
    Children = supervisor:which_children(SupPid),
    {?LOBBY, ChPid, _, _} = lists:keyfind(?LOBBY, 1, Children),
    ChPid.

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
    {ok, {{one_for_one, 0, 1000}, [?CHILD(?LOBBY, worker)]}}.

