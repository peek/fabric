%%%-------------------------------------------------------------------
%%% @author Jonas Wennerstrom <jowe3183@student.uu.se>
%%% @copyright (C) 2013, Jonas Wennerstrom
%%% @doc
%%% A gen_server lobby for managing free-standing game modules. 
%%%
%%% @end
%%% Created :  3 May 2013 by Jonas Wennerstrom <jowe3183@student.uu.se>
%%%-------------------------------------------------------------------
-module(game_lobby).

-behaviour(gen_server).

-include_lib("eunit/include/eunit.hrl").

%% API
-export([start_link/1]).
-export([stop/1]).
-export([new/1]).
-export([join/1]).
-export([join/2]).
-export([leave/1]).
-export([gamelist/1]).
-export([gameboards/1]).
-export([gamerules/1]).
-export([connect/1]).
-export([disconnect/1]).
-export([reset/1]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
	 terminate/2, code_change/3]).

-define(SERVER, ?MODULE). 

-record(state, {mod, 
		minplayers, 
		maxplayers,
		boards, 
		active,
		users}
       ).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server. Mod is the atomic name of the module to be managed
%% by the lobby.
%%
%% @spec start_link(Mod) -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link(Mod) ->
    gen_server:start_link(?MODULE, Mod, []).

%%--------------------------------------------------------------------
%% @doc
%% Terminates Server
%%
%% @spec stop(Server) -> void
%% @end
%%--------------------------------------------------------------------
stop(Server) ->
    gen_server:cast(stop, Server).

%%--------------------------------------------------------------------
%% @doc
%% Handles request for caller to create and be linked with a new board
%% on Server.
%%
%% @spec new(Server) -> {ok, BoardId}
%% @end
%%--------------------------------------------------------------------
new(Server) ->
    gen_server:call(Server, {join, {-1, self()}}).

%%--------------------------------------------------------------------
%% @doc
%% Handles request for caller to be linked with the first available 
%% board hosted by Server.
%%
%% @spec join(Server) -> {ok, BoardId}
%% @end
%%--------------------------------------------------------------------
join(Server) ->
    gen_server:call(Server, {join, {self()}}).


%%--------------------------------------------------------------------
%% @doc
%% Handles request for caller to be linked with board BoardId hosted by 
%% Server. If 0 > BoardId, the caller is linked with a new board.
%%
%% @spec join(BoardId, Server) -> {ok, BoardId} |
%%                                       error
%% @end
%%--------------------------------------------------------------------
join(BoardId, Server) ->
    gen_server:call(Server, {join, {BoardId, self()}}).

%%--------------------------------------------------------------------
%% @doc
%% Handles request to leave the board the caller is connected to on 
%% Server.
%%
%% @spec leave(Server) -> ok | error
%% @end
%%--------------------------------------------------------------------
leave(Server) ->
    gen_server:call(Server, {leave, self()}).

%%--------------------------------------------------------------------
%% @doc
%% Returns information on the games hosted by Server: Name of the module
%% and a Count of the number of people in a game or the lobby
%%
%% @spec gamelist(Server) -> {Name, Count}
%% @end
%%--------------------------------------------------------------------
gamelist(Server) ->
    gen_server:call(Server, users).

%%--------------------------------------------------------------------
%% @doc
%% Returns information on the games hosted by Server: Name of the module
%% and a List of all boards.
%%
%% @spec gameboards(Server) -> {Name, List}
%% @end
%%--------------------------------------------------------------------
gameboards(Server) ->
    gen_server:call(Server, list).

%%--------------------------------------------------------------------
%% @doc
%% Returns information on the games hosted by Server: Name of the module
%% and the rules of the game.
%%
%% @spec gamerules(Server) -> {Name, Rules}
%% @end
%%--------------------------------------------------------------------
gamerules(Server) ->
    gen_server:call(Server, rules).

%%--------------------------------------------------------------------
%% @doc
%% Updates internal state of Server to reflect that calling process has 
%% connected.
%%
%% @spec connect(Server) -> ok
%% @end
%%--------------------------------------------------------------------
connect(Server) ->
    gen_server:cast(Server, {connect, self()}).

%%--------------------------------------------------------------------
%% @doc
%% Updates internal state of Server to reflect that calling process has 
%% disconnected, notifying any game instance the caller is connected to.
%%
%% @spec disconnect(Server) -> ok
%% @end
%%--------------------------------------------------------------------
disconnect(Server) ->
    gen_server:cast(Server, {disconnect, self()}).


%%--------------------------------------------------------------------
%% @doc
%% Sets calling process linked to undefined, reflecting that it is not
%% linked to a board.
%%
%% @spec reset(Server) -> ok
%% @end
%%--------------------------------------------------------------------

reset(Server) ->
    gen_server:cast(Server, {reset, self()}).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server. Mod must be the name of the module to be 
%% managed.
%%
%% @spec init(Mod) -> {ok, State} |
%%                    {ok, State, Timeout} |
%%                    ignore |
%%                    {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init(Mod) ->
    process_flag(trap_exit, true),
    {Min, Max} = Mod:players(),
    {ok, #state{mod=Mod, 
		minplayers=Min,
		maxplayers=Max,
		boards=sets:new(), 
		active=sets:new(), 
		users=sets:new()
	       }
    }.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages. For some requests it is required that 
%% top-level requester has called connect/2
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
%Handles request for a list of all boards, active and waiting
handle_call(list, _From, State) ->
    Mod = State#state.mod,
    BoardList = sets:to_list(State#state.boards),
    ActiveList = sets:to_list(State#state.active),
    MappedB = lists:sort(lists:map(fun({Id, Players}) -> 
					   {Id, Players, false} 
				   end, 
				   BoardList)
			),
    MappedA = lists:sort(lists:map(fun({Id, _Pid, Players}) -> 
					   {Id, Players, true} 
				   end, 
				   ActiveList)
			),
    Reply = {Mod:disp_name(), lists:append(MappedB, MappedA)},
    {reply, Reply, State};

%Handles request for number of users in lobby or games.
handle_call(users, _From, State) ->
    Mod = State#state.mod,
    Users = sets:to_list(State#state.users),
    Reply = {Mod:disp_name(), length(Users)},
    {reply, Reply, State};

%Handles request for game rules
handle_call(rules, _From, State) ->
    Mod = State#state.mod,
    Reply = {Mod:disp_name(), Mod:rules()},
    {reply, Reply, State};

%Handles request to join a new game (defined as table <1)
handle_call({join, {I, Pid}}, _From, State) when I < 1 ->
    Boards = State#state.boards,
    Active = State#state.active,
    Users = State#state.users,
    {_, _, Ref} = lists:keyfind(Pid, 1, sets:to_list(Users)),
    %Finds the smallest unused N >=1
    BoardId = find_smallest({Boards, Active}),
    NewUsers = del_add({Pid, undefined, Ref}, 
		       {Pid, BoardId, Ref}, 
		       Users
		      ),
    NewBoards = sets:add_element({BoardId, [Pid]}, Boards),
    NewState = State#state{boards=NewBoards, 
			   users=NewUsers},
    {reply, {ok, BoardId}, NewState};

%Handles request from Pid to join table I
handle_call({join, {BoardId, Pid}}, _From, State) ->
    Mod = State#state.mod,
    Boards = State#state.boards,
    Active = State#state.active,
    Users = State#state.users,
    Boardlist = sets:to_list(Boards),
    ActiveList = sets:to_list(Active),
    Userlist = sets:to_list(Users),
    %List of users connected to BoardId
    L = proplists:get_value(BoardId, Boardlist, false),
    %BoardId Pid is linked to
    {_, K, Ref} = lists:keyfind(Pid, 1, Userlist),
    Running = lists:keyfind(BoardId, 2, ActiveList),
    if
	%If the player is not allowed to join the table
	(Running =/= false andalso (State#state.minplayers =:= 
				       State#state.maxplayers))
	orelse K =/= undefined
	orelse L == false ->
	    {reply, error, State};
	Running =:= false ->
	    if
		%% If the game is now full and can start
		length(L) == (State#state.minplayers-1) ->
		    NewList = [Pid|L],
		    case Mod:start_link(NewList) of
			{error, _} ->
			    {reply, error, State};
			{ok, GPid} ->
                            lists:map(fun(Player) ->
                                              user:game_start(Player, GPid)
                                      end,
                                      NewList),
      			    NewActive = sets:add_element({BoardId, GPid, NewList}, 
							 Active),
			    NewBoards = sets:del_element({BoardId, L}, Boards),
			    NewUsers = del_add({Pid, undefined, Ref}, 
					       {Pid, BoardId, Ref}, 
					       Users),
			    NewState = State#state{boards=NewBoards,
						   active=NewActive,
						   users=NewUsers},
			    Mod:start_game(GPid),
			    {reply, {ok, BoardId}, NewState}
		    end;
		%% If the game still needs more players.
		true ->
		    L2 = lists:append(L, [Pid]),
		    NewBoards = del_add({BoardId, L}, 
					{BoardId, L2}, 
					Boards),
		    NewUsers = del_add({Pid, undefined, Ref}, 
				       {Pid, BoardId, Ref}, 
				       Users),
		    NewState = State#state{boards=NewBoards,
					   users=NewUsers},
		    {reply, {ok, BoardId}, NewState}
	    end;    
	%Board I is active
	true ->
	    {_, Game, Players} = Running,
	    case Mod:connect(Pid, Game) of
		ok ->
		    NewActive = del_add(Running, 
					{BoardId, Game, [Pid, Players]}, 
					Active),
		    NewUsers = del_add({Pid, undefined, Ref},
				       {Pid, BoardId, Ref},
				       Users),
		    NewState = State#state{active=NewActive,
					   users=NewUsers},
		    {reply, {ok, BoardId}, NewState};
		error ->
		    {reply, error, State}
	    end
    end;

%Handles request to leave a board.
handle_call({leave, Pid}, _From, State) ->
    Boards = State#state.boards,
    Users  = State#state.users,
    {Reply, B2, U2} = update_user(Pid, undefined, Boards, Users),
    NewState = State#state{boards=B2, 
			   users=U2},
    {reply, Reply, NewState};
    
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages. For some casts calling process is required 
%% to have previously called connect/2.
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
%Handles request to add Pid to users
handle_cast({connect, Pid}, State) ->
    Users = State#state.users,
    NewUsers = sets:add_element({Pid, undefined, monitor(process, Pid)}, Users),
    NewState = State#state{users=NewUsers},
    {noreply, NewState};
%Handles request to remove Pid from users
handle_cast({disconnect, Pid}, State) ->
    Boards = State#state.boards,
    Users = State#state.users,
    Active = State#state.active,
    {_, I, Ref} = lists:keyfind(Pid, 1,
				sets:to_list(Users)
			       ),
    Tuple = lists:keyfind(I, 1, 
			  sets:to_list(Active)
			 ),
    if
	Tuple == false ->
	    {_, NewBoards, NewUsers} = update_user(Pid, delete, Boards, Users),
	    NewState = State#state{boards=NewBoards, 
				   users=NewUsers},
	    {noreply, NewState};
	true->
	    Mod = State#state.mod,
	    {I, GPid, L} = Tuple,
	    Mod:disconnect(Pid, GPid),
	    NewUsers = sets:del_element({Pid, I, Ref}, Users),
	    demonitor(Ref, [flush]),
	    NewActive = del_add({I, GPid, L}, 
				{I, GPid, lists:delete(Pid, L)}, 
				Active),
	    NewState = State#state{active=NewActive,
				   users=NewUsers},
	    {noreply, NewState}
    end;

%Handles request to reset Pid to be linked with undefined
handle_cast({reset, Pid}, State) ->
    Mod = State#state.mod,
    Users = State#state.users,
    Active = State#state.active,
    UserTuple = lists:keyfind(Pid, 1,
			      sets:to_list(Users)
			     ),
    if
	UserTuple == false ->
	    {noreply, State};
	true ->
	    {_, I, Ref} = UserTuple,
	    NewUsers = del_add({Pid, I, Ref}, 
			       {Pid, undefined, Ref}, 
			       Users),
	    ActiveTuple = lists:keyfind(I, 1, 
					sets:to_list(Active)
				       ),
	    if
		ActiveTuple == false ->
		    NewState = State#state{users=NewUsers},
		    {noreply, NewState};
		true ->
		    {I, GPid, L} = ActiveTuple,
		    NewActive = del_add({I, GPid, L}, 
					{I, GPid, lists:delete(Pid, L)}, 
					Active),
		    Mod:disconnect(Pid, GPid),
		    NewState = State#state{users=NewUsers,
					  active=NewActive},
		    {noreply, NewState}
		end
    end;

%Handles request to stop the server
handle_cast(stop, State) ->
    {stop, normal, State};
handle_cast(_, _) ->
    ok.
			    

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
%Handles the case where a game instance exits.
handle_info({'EXIT', Pid, _R}, State) ->
    Users = State#state.users,
    Active = State#state.active,
    ActiveElement = lists:keyfind(Pid, 2, 
				  sets:to_list(Active)
				 ),
    {_, _, Players} = ActiveElement,
    NewUsers = reset_all(Players, Users),
    NewActive = sets:del_element(ActiveElement, Active),
    NewState = State#state{active=NewActive,
			   users=NewUsers},
    {noreply, NewState};
handle_info({'DOWN', _Ref, process, Pid, _Reason}, State) ->
    gen_server:cast(self(), {disconnect, Pid}),
    {noreply, State};
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Removes element {Pid, _} from Users, then returns Usrs with element
%% {Pid, BoardId} added unless BoardId is the atom delete.
%%
%% @spec update_user(Pid, BoardId, Boards, Users) -> 
%%          {ok, NewBoards, NewUsers} |
%%          {error, Boards, Users}
%% @end
%%--------------------------------------------------------------------
update_user(Pid, BoardId, Boards, Users) ->
    {_, I, Ref} = lists:keyfind(Pid, 1,
				sets:to_list(Users)
			       ),  
    if
	BoardId == delete ->
	    NewUsers = sets:del_element({Pid, I, Ref}, Users),
	    demonitor(Ref, [flush]);
	true ->
	    NewUsers = del_add({Pid, I, Ref},
			       {Pid, BoardId, Ref},
			       Users
			      )
    end,
    {Reply, NewBoards} = update_aux(Pid, I, Boards),
     if
	Reply == error ->
	    {error, Boards, Users};
	true ->
	    {ok, NewBoards, NewUsers}
    end.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Help function to update_user/4. Returns {ok, Boards} if I is
%% undefined, else tries to remove Pid from list of linked Users to
%% board I in Boards.
%%
%% @spec update_aux(Pid, I, Users) -> {ok, Boards}    | 
%%                                    {ok, NewBoards} | 
%%                                    {error, Boards}
%% @end
%%--------------------------------------------------------------------
update_aux(_Pid, undefined, Boards) ->
    {ok, Boards};
update_aux(Pid, I, Boards) ->
    L = proplists:get_value(I, 
			    sets:to_list(Boards)
			   ),
    if
	L == undefined ->
	    {error, Boards};
	true ->
	    L2 = lists:delete(Pid, L),
	    if
		%Board is empty and can be deleted
		L2 == [] ->
		    {ok, sets:del_element({I, L}, Boards)};
		%There are still users linked with the table
		true ->
		    NewBoards = del_add({I, L}, 
					{I, L2}, 
					Boards
				       ),
		    {ok, NewBoards}
	    end
    end.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Recursively removes all members of the list Player from Users, then
%% adds them in the "waiting" state - linked with undefined in Users.
%%
%% @spec reset_all(Players, Users) -> NewUsers
%% @end
%%--------------------------------------------------------------------
reset_all([], Users) ->
    Users;
reset_all([H|T], Users) ->
    {_, I, Ref} = lists:keyfind(H, 1,
				sets:to_list(Users)
			       ),
    NewUsers = del_add({H, I, Ref}, 
		       {H, undefined, Ref}, 
		       Users),
    reset_all(T, NewUsers).


%%--------------------------------------------------------------------
%% @private
%% @doc
%% Returns the smallest int >=1 not used as a key in Boards or Running.
%%
%% @spec find_smallest({Boards, Running}) -> Int
%% @end
%%--------------------------------------------------------------------
find_smallest({Boards, Running}) ->
    RList = lists:map(fun({Id, _Pid, _Players}) ->
				    Id end, 
			     sets:to_list(Running)
			    ),
    {BList, _} = lists:unzip(
		   sets:to_list(Boards)
		  ),
    find_smallest(
      lists:sort(
	lists:merge(BList, RList)
       ), 1
     ).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Returns the smallest integer >= N not found in Idlist. Idlist must 
%% be sorted.
%%
%% @spec find_smallest(Idlist, N) -> Int
%% @end
%%--------------------------------------------------------------------
find_smallest([], N) ->
    N;
find_smallest(Idlist, N) ->
    if
	hd(Idlist) == N ->
	    find_smallest(tl(Idlist), N+1);
	true ->
	    N
    end.
    
%%%===================================================================
%%% Internal API
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handles request to update Set deleting Del and adding Add.
%%
%% @spec del_add(Del, Add, Set) -> NewSet
%% @end
%%--------------------------------------------------------------------
del_add(Del, Add, Set) ->
    S2 = sets:del_element(Del, Set),
    sets:add_element(Add, S2).


