-module(fabric_app).

-behaviour(application).

%% Application callbacks
-export([start/2]).
-export([stop/1]).

-define(LISTENER_REF, http).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    {ok, SupPid} = fabric_sup:start_link(),
    Lobby = fabric_sup:lobby_pid(SupPid),
    Dispatch = cowboy_router:compile(
                 [{'_',
                   [{"/", cowboy_static,
                     [{directory, {priv_dir, fabric, [<<"www">>]}},
                      {file, <<"index.html">>},
                      {mimetypes,
                       [{<<".html">>, [<<"text/html">>]}]}
                     ]},
                    {"/connect", user,
                     [{lobby, Lobby}]},
                    {"/static/bullet/[...]", cowboy_static,
                     [{directory, {priv_dir, bullet, []}},
                      {mimetypes,
                       [{<<".js">>, [<<"application/javascript">>]}]}
                     ]},
                    {"/static/fabric/[...]", cowboy_static,
                     [{directory, {priv_dir, fabric, [<<"www">>]}},
                      {mimetypes,
                       [{<<".html">>, [<<"text/html">>]},
                        {<<".css">>, [<<"text/css">>]},
                        {<<".js">>, [<<"application/javascript">>]}
                       ]}
                     ]}
                   ]}
                 ]),
    cowboy:start_http(?LISTENER_REF, 100, [{port, 8080}],
                      [{env, [{dispatch, Dispatch}]}]),
    {ok, SupPid}.

stop(_State) ->
    cowboy:stop_listener(?LISTENER_REF),
    ok.
