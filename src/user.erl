%%%-------------------------------------------------------------------
%%% @author Per Ekemark <per@EKEMPHV4>
%%% @copyright (C) 2013, Per Ekemark
%%% @doc
%%% Implements a cowboy_websocket_handler which receives messages
%%% from the lient and responds in an appropriate way if neccesary.
%%% 
%%% @end
%%% Created :  3 May 2013 by Per Ekemark <per@EKEMPHV4>
%%%-------------------------------------------------------------------
-module(user).
-behaviour(cowboy_websocket_handler).

%% API
-export([send/2]).
-export([game/2]).
-export([game_start/2]).
-export([game_stop/1]).

%% cowboy_websocket_handler callbacks
-export([init/3]).
-export([websocket_init/3]).
-export([websocket_handle/3]).
-export([websocket_info/3]).
-export([websocket_terminate/3]).

-include("protocol.hrl").
-define(MSEC_TO_SEC(MSec), MSec * 1000000).
-define(SEC_TO_MILIS(Sec), Sec * 1000).
-define(MSEC_TO_MILIS(MSec), ?SEC_TO_MILIS(?MSEC_TO_SEC(MSec))).
-define(MICRS_TO_MILIS(MicrS), MicrS div 1000).

-record(state, {lobby = undefined          :: undefined | pid(),
                game_lobby = undefined     :: undefined | pid(),
                game_lobby_mon = undefined :: undefined | reference(),
                game = undefined           :: undefined | pid(),
                game_mon = undefined       :: undefined | reference(),
                game_name = undefined      :: undefined | atom()
               }).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Sends an asynchronous message to the client associated with Pid.
%% Data should be a Json object.
%% A field "time" will be added with the time of transmission.
%% Note: if there already are "time" fields they will be removed.
%% @end
%%--------------------------------------------------------------------
-spec send(Pid :: pid(), Data :: jsx:json_term()) ->
                  ok | {error, Reason :: term()}.
send(Pid, Data) ->
    case jsx:is_json(Data) of
        false ->
            {error, "Not JSON"};
        true ->
            Pid ! {send, Data},
            ok
    end.

%%--------------------------------------------------------------------
%% @doc
%% Sends the content of Data, wraped in a protocol Json object, to the
%% client associated with Pid. Data can either be a Json object, a
%% binary utf-8 encoded string or a number.
%% @end
%%--------------------------------------------------------------------
-spec game(Pid :: pid(), Data :: jsx:json_term() |
                                 binary() |
                                 number()) ->
                  ok | {error, Reason :: term()}.
game(Pid, Data) ->
    case jsx:is_json(Data) orelse
         is_binary(Data) orelse
         is_number(Data) of
        false ->
            {error, "Data not valid"};
        true ->
            Pid ! {game, Data},
            ok
    end.

%%--------------------------------------------------------------------
%% @doc
%% Sends a message to the client to let it know that the game is
%% started. Pid is the pid of the user process, GPid is the pid of
%% the game process.
%% @end
%%--------------------------------------------------------------------
-spec game_start(Pid :: pid(), GPid :: pid()) -> ok.
game_start(Pid, GPid) ->
    Pid ! {game_start, GPid},
    ok.

%%--------------------------------------------------------------------
%% @doc
%% Messages the client that the game has shut down. Pid is the pid of
%% the user process.
%% @end
%%--------------------------------------------------------------------
-spec game_stop(Pid :: pid()) -> ok.
game_stop(Pid) ->
    Pid ! game_stop,
    ok.

%%%===================================================================
%%% cowboy_websocket_handler callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Upgrades the connection to a websocket
%%
%% @spec init(TransportName, Req, Opts) -> {upgrade,
%%                                      protocol,
%%                                      cowboy_websocket}
%% @end
%%--------------------------------------------------------------------
init({tcp, http}, _Req, _Opts) ->
    {upgrade, protocol, cowboy_websocket}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes a connection
%%
%% @spec websocket_init(TransportName, Req, Opts) -> {ok, Req, State}
%% @end
%%--------------------------------------------------------------------
websocket_init(_TransportName, Req, Opts) ->
    Lobby = proplists:get_value(lobby, Opts),
    {ok, Req, #state{lobby = Lobby}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handles incomming data
%%
%% @spec websocket_handle(Data, Req, State) ->
%%           {ok, Req, State} |
%%           {reply, Reply, Req, State}
%% @end
%%--------------------------------------------------------------------
websocket_handle({text, Data}, Req, State) ->
    case jsx:is_json(Data) of
        false -> {ok, Req, State};
        true ->
            Decode = jsx:decode(Data),
            case proplists:get_value(?TYPE_FIELD, Decode) of 
                ?TYPE_LOBBY ->
                    lobby_msg(Decode, Req, State);
                ?TYPE_GAME_LOBBY ->
                    game_lobby_msg(Decode, Req, State);
                ?TYPE_GAME ->
                    game_msg(Decode, Req, State);
                _ ->
                    {ok, Req, State}
            end
    end;
websocket_handle(_Data, Req, State) ->
    {ok, Req, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handles messages sent to the handler
%%
%% @spec websocket_info(Info, Req, State) ->
%%           {ok, Req, State} |
%%           {reply, Reply, Req, State}
%% @end
%%--------------------------------------------------------------------
websocket_info({send, Data}, Req, State) ->
    {reply, {text, json_stamp(Data)}, Req, State};
websocket_info({game, Data}, Req, State) ->
    Json = [{?TYPE_FIELD, ?TYPE_GAME},
            {?INFO_FIELD, ?INFO_GAME_MSG},
            {?GAME_FIELD, atom_to_binary(State#state.game_name, utf8)},
            {?GAME_DATA_FIELD, Data}],
    {reply, {text, json_stamp(Json)}, Req, State};
websocket_info({game_start, GPid}, Req, State) ->
    Ref = monitor(process, GPid),
    Json = [{?TYPE_FIELD, ?TYPE_GAME_LOBBY},
            {?INFO_FIELD, ?INFO_GAME_START},
            {?GAME_FIELD, atom_to_binary(State#state.game_name, utf8)}],
    {reply, {text, json_stamp(Json)}, Req, State#state{game = GPid, game_mon = Ref}};
websocket_info(game_stop, Req, State) ->
    demonitor(State#state.game_mon, [flush]),
    game_lobby:reset(State#state.game_lobby),
    Json = [{?TYPE_FIELD, ?TYPE_GAME},
            {?INFO_FIELD, ?INFO_KICK_TO_GAME_LOBBY},
            {?GAME_FIELD, atom_to_binary(State#state.game_name, utf8)}],
    {reply, json_stamp(Json), Req, State#state{game=undefined, game_mon=undefined}};
websocket_info({'DOWN', Ref, process, _Pid, _Reason}, Req, State) when State#state.game_lobby_mon =:= Ref ->
    Json = [{?TYPE_FIELD, ?TYPE_GAME_LOBBY},
            {?INFO_FIELD, ?INFO_KICK_TO_LOBBY}],
    State2 = State#state{game_lobby = undefined,
                         game_lobby_mon = undefined,
                         game = undefined,
                         game_mon = undefined,
                         game_name = undefined},
    {reply, {text, json_stamp(Json)}, Req, State2};
websocket_info({'DOWN', Ref, process, _Pid, _Reason}, Req, State) when State#state.game_mon =:= Ref ->
    Json = [{?TYPE_FIELD, ?TYPE_GAME},
            {?INFO_FIELD, ?INFO_KICK_TO_GAME_LOBBY}],
    State2 = State#state{game = undefined,
                         game_mon = undefined},
    {reply, {text, json_stamp(Json)}, Req, State2};
websocket_info(_Info, Req, State) ->
    {ok, Req, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Terminates the connection
%%
%% @spec websocket_terminate(Reason, Req, State) -> ok
%% @end
%%--------------------------------------------------------------------
websocket_terminate(_Reason, _Req, _State) ->
    ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================

json_stamp(Data) ->
    Msg = jsx:encode(timestamp(Data)),
    Msg.

timestamp(Data) ->
    Data2 = proplists:delete(?TIME_FIELD, Data),
    {MSec, Sec, MicrS} = now(),
    Time = ?MSEC_TO_MILIS(MSec) + ?SEC_TO_MILIS(Sec) + ?MICRS_TO_MILIS(MicrS),
    [{?TIME_FIELD, Time}|Data2].

lobby_msg(Data, Req, State) ->
    case proplists:get_value(?INFO_FIELD, Data) of
        ?INFO_LIST_GAMES ->
            Games = lobby:game_list(State#state.lobby),
            Games2 = lists:map(fun({G, P}) ->
                                       [{?GAMES_NAME_FIELD, atom_to_binary(G, utf8)},
                                        {?GAMES_PLAYERS_FIELD, P}]
                               end,
                               Games),
            Reply = [{?TYPE_FIELD, ?TYPE_LOBBY},
                     {?INFO_FIELD, ?INFO_GAME_LIST},
                     {?GAMES_FIELD, Games2}],
            {reply, {text, json_stamp(Reply)}, Req, State};
        ?INFO_TO_GAME_LOBBY ->
            GameBinary = proplists:get_value(?GAME_FIELD, Data),
            Game = binary_to_atom(GameBinary, utf8),
            {Answer, State2} =
                case lobby:join_game_lobby(State#state.lobby, Game) of
                    {ok, Pid} ->
                        Ref = monitor(process, Pid),
                        game_lobby:connect(Pid),
                        {?INFO_GAME_LOBBY_ACCEPT,
                         State#state{game_lobby=Pid, game_lobby_mon=Ref, game_name=Game}};
                    {error, _} ->
                        {?INFO_GAME_LOBBY_ERROR, State}
                end,
            Reply = [{?TYPE_FIELD, ?TYPE_LOBBY},
                     {?INFO_FIELD, Answer},
                     {?GAME_FIELD, GameBinary}],
            {reply, {text, json_stamp(Reply)}, Req, State2}
    end.

game_lobby_msg(Data, Req, State) -> 
    case proplists:get_value(?INFO_FIELD, Data) of
        ?INFO_GAME_BOARDS ->
            {_GName, Boards} = game_lobby:gameboards(State#state.game_lobby),
            Boards2 =
                lists:map(fun({Id, Players, Started}) ->
                                  [{?BOARDS_ID_FIELD, Id},
                                   {?BOARDS_PLAYERS_FIELD, length(Players)},
                                   {?BOARDS_STARTED_FIELD, Started}]
                          end,
                          Boards),
            Reply = [{?TYPE_FIELD, ?TYPE_GAME_LOBBY},
                     {?INFO_FIELD, ?INFO_GAME_BOARDS},
                     {?GAME_FIELD, atom_to_binary(State#state.game_name, utf8)},
                     {?BOARDS_FIELD, Boards2}],
            {reply, {text, json_stamp(Reply)}, Req, State};
        ?INFO_GAME_RULES ->
            {_GName, Rules} = game_lobby:gamerules(State#state.game_lobby),
            Reply = [{?TYPE_FIELD, ?TYPE_GAME_LOBBY},
                     {?INFO_FIELD, ?INFO_GAME_RULES},
                     {?GAME_FIELD, atom_to_binary(State#state.game_name, utf8)},
                     {?RULES_FIELD, list_to_binary(Rules)}],
            {reply, {text, json_stamp(Reply)}, Req, State};
        ?INFO_JOIN_GAME ->
            Board = proplists:get_value(?BOARD_FIELD, Data),
            BoardRes = case Board < 0 of
                           true ->
                               game_lobby:new(State#state.game_lobby);
                           false ->
                               game_lobby:join(Board, State#state.game_lobby)
                       end,
            Id = case BoardRes of
                     error -> -1;
                     {ok, BoardId} -> BoardId
                 end,
            Reply = [{?TYPE_FIELD, ?TYPE_GAME_LOBBY},
                     {?INFO_FIELD, ?INFO_JOIN_GAME},
                     {?GAME_FIELD, atom_to_binary(State#state.game_name, utf8)},
                     {?BOARD_FIELD, Id}],
            {reply, {text, json_stamp(Reply)}, Req, State};
        ?INFO_LEAVE_GAME ->
            Leave = case game_lobby:leave(State#state.game_lobby) of
                        ok -> ?INFO_LEAVE_GAME_ACCEPT;
                        error -> ?INFO_LEAVE_GAME_DENY
                    end,
            Reply = [{?TYPE_FIELD, ?TYPE_GAME_LOBBY},
                     {?INFO_FIELD, Leave},
                     {?GAME_FIELD, atom_to_binary(State#state.game_name, utf8)}],
            {reply, {text, json_stamp(Reply)}, Req, State};
        ?INFO_LEAVE_GAME_LOBBY ->
            game_lobby:disconnect(State#state.game_lobby),
            demonitor(State#state.game_lobby_mon, [flush]),
            Reply = [{?TYPE_FIELD, ?TYPE_GAME_LOBBY},
                     {?INFO_FIELD, ?INFO_KICK_TO_LOBBY}],
            State2 = State#state{game_lobby = undefined,
                                 game_lobby_mon = undefined,
                                 game_name = undefined},
            {reply, {text, json_stamp(Reply)}, Req, State2}
    end.

game_msg(Data, Req, State) ->
    case proplists:get_value(?INFO_FIELD, Data) of
        ?INFO_GAME_MSG ->
            Module = State#state.game_name,
            Module:receive_msg(State#state.game,
                               proplists:get_value(?GAME_DATA_FIELD, Data)),
            {ok, Req, State};
        ?INFO_LEAVE_THE_GAME ->
            Module = State#state.game_name,
            Module:disconnect(self(), State#state.game),
            demonitor(State#state.game_mon, [flush]),
            Reply = [{?TYPE_FIELD, ?TYPE_GAME},
                     {?INFO_FIELD, ?INFO_KICK_TO_GAME_LOBBY},
                     {?GAME_FIELD, atom_to_binary(State#state.game_name, utf8)}],
            State2 = State#state{game = undefined,
                                 game_mon = undefined},
            {reply, {text, json_stamp(Reply)}, Req, State2}
    end.
