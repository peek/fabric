%%%-------------------------------------------------------------------
%%% @author Per Ekemark <per@EKEMPHV4>
%%% @copyright (C) 2013, Per Ekemark
%%% @doc
%%%
%%% @end
%%% Created : 15 May 2013 by Per Ekemark <per@EKEMPHV4>
%%%-------------------------------------------------------------------
-module(game_lobby_sup).

-behaviour(supervisor).

%% API
-export([start_link/1]).
-export([game_list/1]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the supervisor with one game_lobby for each game in the
%% list of Games. The element in games should the module names
%% for the games entry module (that implements behaviour game).
%%
%% @spec start_link(Games) -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link(Games) ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, Games).

%%--------------------------------------------------------------------
%% @doc
%% Returns a tupple list [{Game, Pid}] with all gamelobbys currently
%% running. Game is the game module that the game_lobby serves and
%% Pid is the pid of said game_lobby. SupPid is the pid of the
%% supervisor.
%%
%% @spec game_list(SupPid) -> {ok, GameList} | {error, restarting}
%% @end
%%--------------------------------------------------------------------
game_list(SupPid) ->
    Children = supervisor:which_children(SupPid),
    case lists:keymember(restarting, 2, Children) of
        true ->
            {error, restarting};
        false ->
            {ok, lists:map(fun({Game, Pid, _, _}) ->
                                   {Game, Pid}
                           end,
                           Children)}
    end.

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a supervisor is started using supervisor:start_link/[2,3],
%% this function is called by the new process to find out about
%% restart strategy, maximum restart frequency and child
%% specifications.
%%
%% @spec init(Args) -> {ok, {SupFlags, [ChildSpec]}} |
%%                     ignore |
%%                     {error, Reason}
%% @end
%%--------------------------------------------------------------------
init(Games) ->
    RestartStrategy = one_for_one,
    MaxRestarts = 1000,
    MaxSecondsBetweenRestarts = 3600,

    SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

    Restart = permanent,
    Shutdown = 2000,
    Type = worker,

    Children = lists:map(fun(Game) ->
                                 {Game,
                                  {game_lobby, start_link, [Game]},
                                  Restart,
                                  Shutdown,
                                  Type,
                                  [game_lobby]}
                         end,
                         Games),

    {ok, {SupFlags, Children}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
