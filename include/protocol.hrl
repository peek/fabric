%% Base Protocol Json definitions 

%% Type field
-define(TYPE_FIELD, <<"type">>).
%% Type content
-define(TYPE_LOBBY, <<"lobby">>).
-define(TYPE_GAME_LOBBY, <<"gameLobby">>).
-define(TYPE_CHAT, <<"chat">>).
-define(TYPE_GAME, <<"game">>).

%% Time field
-define(TIME_FIELD, <<"time">>).

%% Info field
-define(INFO_FIELD, <<"info">>).
%% Info content
-define(INFO_LIST_GAMES, <<"listGames">>).
-define(INFO_GAME_LIST, <<"gameList">>).
-define(INFO_TO_GAME_LOBBY, <<"toGameLobby">>).
-define(INFO_GAME_LOBBY_ACCEPT, <<"gameLobbyAccept">>).
-define(INFO_GAME_LOBBY_ERROR, <<"gameLobbyError">>).
-define(INFO_GAME_BOARDS, <<"gameBoards">>).
-define(INFO_GAME_RULES, <<"gameRules">>).
-define(INFO_JOIN_GAME, <<"joinGame">>).
-define(INFO_LEAVE_GAME, <<"leaveGame">>).
-define(INFO_LEAVE_GAME_ACCEPT, <<"leaveGameAccept">>).
-define(INFO_LEAVE_GAME_DENY, <<"leaveGameDeny">>).
-define(INFO_GAME_MSG, <<"gameMsg">>).
-define(INFO_GAME_START, <<"gameStart">>).
-define(INFO_LEAVE_THE_GAME, <<"leaveGame">>).
-define(INFO_LEAVE_GAME_LOBBY, <<"leaveGameLobby">>).
-define(INFO_KICK_TO_LOBBY, <<"kickToLobby">>).
-define(INFO_KICK_TO_GAME_LOBBY, <<"kickToGameLobby">>).

%% Games field
-define(GAMES_FIELD, <<"games">>).
%% Games content
-define(GAMES_NAME_FIELD, <<"name">>).
-define(GAMES_PLAYERS_FIELD, <<"players">>).

%% Game field
-define(GAME_FIELD, <<"game">>).

%% Boards field
-define(BOARDS_FIELD, <<"boards">>).
%% Boards content
-define(BOARDS_ID_FIELD, <<"id">>).
-define(BOARDS_PLAYERS_FIELD, <<"players">>).
-define(BOARDS_STARTED_FIELD, <<"started">>).

%% Rules field
-define(RULES_FIELD, <<"rules">>).

%% Msg field
-define(MSG_FIELD, <<"msg">>).

%% Msg time field
-define(MSG_TIME_FIELD, <<"msgTime">>).

%% Disp name field
-define(DISP_NAME_FIELD, <<"dispName">>).

%% Game data field
-define(GAME_DATA_FIELD, <<"gameData">>).

%% Board field
-define(BOARD_FIELD, <<"board">>).

