==== COURSE ==== 

Operating System and Multicore programming (1DT089) Spring 2013

Department of Information Technology 
Uppsala university


==== GROUP ==== 

14.


==== PROJECT NAME ==== 

Let The Games Begin!


==== PROJECT DESCRIPTION ==== 

This project aims to create a server framework designed to host a
multitude of concurrent games developed by third parties.


==== GROUP MEMBERS ==== 

901006-0219 Viktor.Bostrand.5741@student.uu.se
920406-5156 Per.Ekemark.8846@student.uu.se
870719-0172 Jesper.Svennebring.6757@student.uu.se
910628-4012 Adam.Ulander.2131@student.uu.se
890530-7032 Jonas.Wennerstrom.3183@student.uu.se


==== MAY THE SOURCE BE WITH YOU ==== 

The latest version of the project may be found at: 
https://bitbucket.org/peek/fabric

==== ERLANG VERSION ====

This software was developed using Erlang R16B.
     	      	  	    	       
==== MAKE IT HAPPEN ==== 

Using the make utility you can perform the following actions:

make         ==> Compiles the Erlang source files if necessary. 
make quick   ==> As above, excluding dependencies
make clean   ==> Removes all beam files, crash logs, temporary files and Edoc generated html files.
make doc     ==> Generates Edoc documentation in the doc/html directory.
make start   ==> Starts the system.
make archive ==> Generates an archive (.tar.gz) of the source files (everything needed to restore
                   the system to a working state).


==== TO COMPILE ==== 

To compile the project, simply type make and press enter.
Alternatively, type make quick to skip compiling dependencies.


==== TO RUN THE SYSTEM ==== 

To start the system, type make start and press enter. 
The server is now available at <host_ip>:8080.
