REBAR    = ./rebar
ERLANG   = erl
EFLAGS   = -pa ebin \
	   -pa deps/*/ebin \
	   -config priv/app/config/sys.config \
	   -s fabric
NODEPS   = skip_deps=true
TAR      = tar
TARFLAGS = czvf
TARNAME  = OSM_2013_group_14_final_deliverable__$(shell date "+%Y-%m-%d__%H-%M-%S")__.tar.gz

fabric: deps build

build:
	$(REBAR) compile

clean:
	$(REBAR) clean
	rm -f *.dump
	find ./ -name '*~' | xargs rm -f
	rm -f doc/html/*

deps:
	$(REBAR) get-deps

clear_deps:
	rm -rf deps

start:
	$(ERLANG) $(EFLAGS)

doc:
	$(REBAR) doc $(NODEPS)

quick:
	$(REBAR) compile $(NODEPS)

archive: clean
	$(TAR) $(TARFLAGS) $(TARNAME) src priv include doc/pdf rebar rebar.config Makefile README.txt

.PHONY: fabric build clean start clear_deps doc quick archive
