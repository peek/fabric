
/* Frontend game module for guess game */



GuessingGame.prototype = new Game();

GameFactory.register('guessing_game', function() { return new GuessingGame(); });

function GuessingGame() {
    this.enter = function(socket) {
        this.socket = socket;
        $('#appendHere').empty();
        $('#appendHere').append('<textarea class="center" id="guessArea" readonly></textarea><br>');
        $('#appendHere').append('<input class="center" type=text id="guessText" />');
        $('#appendHere').append('<button class="center" type=sumbit id="guessSend">Send</button>');
        var scope = this;
        $('#guessSend').click(function(){
            var txt = $('#guessText').val();
            $('#guessText').val('');
            scope.socket.send(txt);
        });
    };
    this.onmessage = function(message){
        $('#guessArea').append(message + '\n');
    }

}

