
/*
 * This call is used to create a new instance of
 * funtion Game in javascript.js. This will give 
 * access to the funcions:
 * enter (used to start the game) and
 * onmessage (used for communication between the javascripts).  
 */

GameClass.prototype = new Game();

/*
 * This call is used to register a new game and return a 
 * function <GameClass> which will start the game.
 * <Game_Module> should have the same name as the backend 
 * game module, and <GameClass> should have the same name as the 
 * main function in this javascript.
 */

GameFactory.register('game_module', function() { return new GameClass(); });

/*
 * The <GameClass> is the main function in the javascript. It will run 
 * all the code and handle all calls that the game needs. 
 */

function GameClass() {
    /* Defines the enter callback */
    this.enter = function(socket) {
        this.socket = socket;
	/*
	 * All the content of your script, i.e. your UI goes here. 
	 * All content should be appended to $('#appendHere'). 
	 */
        
	/* 
         * Sets variable scope to this, the socket can now
         * be accessed from scope.
         */
	var scope = this;
       
	/* 
	 * This call is used to send user input to the server. 
	 * Data is the data that you want to send.
	 */
        scope.socket.send(data);
        });
    };

    /* 
     *  This call is used to handle incomming 
     * information, from server, to the script. 
     */
    this.onmessage = function(message){
        
    }

}

