/********************************/
/*                              */
/*     Global Variables         */
/*                              */
/********************************/


/*
 *  flag is an int that make sure
 *  that the client will not ask
 *  for the gameList more then
 *  once
 */
var flag = 0;

/*
 *  requestGamesLobby is a Json-object
 *  passed from the client to the
 *  server.
 */
var requestGameLobby;


/*
 *  requestGame is a Json-object
 *  passed from the client to the
 *  server.
 */
var requestGame;


/*
 *  ListAllGames is a Json-object
 *  passed from the client to the
 *  server.
 */
var listAllGames


/*
 *  boardFlag is an int, initially set to 0,
 *  that controles if two keys are pressed at once
 *  the right key will respond
 */
var boardFlag = 0;


/*
 *  startGame is used to start a game.
 */
var startGame;


/*
 *  exitFlag is used for the exitbutton 
 *  that appears when a game starts. Set to 1 when 
 *  a game has started, else 0.
 */
var exitFlag = 0;


/*
 *  exitJson is a Json-object
 *  passed from the client to the
 *  server.
 */
var exitJson;

/********************************/
/*                              */
/*     Global Functions         */
/*                              */
/********************************/



/*
 *  reply_click_gameLobby sets Json-object requestGameLobby to:
 *  type: lobby, time: time, info: string, game: game name
 *  Where game name is the game button that was clicked upon.
 */
function reply_click_gameLobby(clicked_id){
    requestGameLobby = JSON.stringify({type: 'lobby',
                                       time: new Date().getTime(),
                                       info: 'toGameLobby',
                                       game: clicked_id}
                                     );
}


/*
 *  reply_click_gameBoard sets Json-object rquestGame to:
 *  type: gameLobby, time: time, info: string, game: game name, board: boardId
 *  Where game name is the game button that was clicked upon, and boardId
 *  the ID of the board clicked upon.
 */
function reply_click_gameBoard(clicked_id, clicked_value){
    boardFlag = 0;
    console.log(boardFlag);
    requestGame = JSON.stringify({type: 'gameLobby',
                                  time: new Date().getTime(),
                                  info: 'joinGame',
                                  game: clicked_value,
                                  board: parseInt(clicked_id)}
                                );
}


/*
 *  reply_click_back_to_menu sets Json-object exitJson to:
 *  type: gameLobby, time: time, info: leaveGameLobby
 */
function reply_click_back_to_menu(){
    boardFlag = 1;
    console.log(boardFlag);
    exitJson = JSON.stringify({type: 'gameLobby',
                               time: new Date().getTime(),
                               info: 'leaveGameLobby'}
                             );
}


/*
 *  reply_click_back_to_menu sets Json-object exitJson to:
 *  type: game, time: time, info: leaveGame, game: game name
 *  Where game name is the name of the game running.
 */
function reply_click_exit_game(clicked_id){
    if(exitFlag == 0){
        exitJson = JSON.stringify({type: 'gameLobby',
                                   time: new Date().getTime(),
                                   info: 'leaveGame',
                                   game: clicked_id}
                                 );
    } else {
        exitJson = JSON.stringify({type: 'game',
                                   time: new Date().getTime(),
                                   info: 'leaveGame',
                                   game: clicked_id}
                                 );
    } 
}
/*
 *  bool_to_string coverts boolean to string.
 *  When true "Yes" is returned, else "No"
 */
function bool_to_string(bool){
    if(bool == true){
        return "Yes";
    } else {
        return "No";
    }
}

/*
 *  Game is used to controle the game.
 */

function Game() {
    /*
     * enter is used to start the game
     */
    this.enter = function(socket) {};
    /*
     * onmessage is used to pass messages to the game
     */
    this.onmessage = function(message) {};
}

/*
 *  GameSocket sends message from the game to the server
 */

function GameSocket(socket, gameName) {
    this.socket = socket;


    /*
     *  Sends object to server as a Json-object.
     */
    this.send = function(gameData) {
        var envelope = { type: 'game',
                         info: 'gameMsg',
                         game: gameName,
                         gameData: gameData
                       };
        this.socket.send(JSON.stringify(envelope));
    }
}

/*
 *
 *
 *
 */

var GameFactory = (function() {
    var games = {};
    return {

        /*
         *  register a function that create an instance of
         *  the game.
         */
        register: function(name, ctor) {
            console.log('Registered new game ' + name + '.');
            games[name] = ctor;
            return GameFactory;
        },

        /*
         *  unsregister is used to remove game.
         */
        unregister: function(name) {
            delete games[name];
        },

        /*
         *  create adds a game to list games.
         */
        create: function(name) {
            return games[name]();
        }
    }
})();


/* On loaded site */

$(document).ready(function() {
    $('.chat').hide();
    $('.rulesClass').hide();
    $('#boardDiv').hide();
    $('#boardDivExit').hide();
    $('#waitingForPlayers').hide();

    /*
     * Initiates a websocket.
     */
    var socket = new WebSocket('ws://' + document.location.host + '/connect');

    /*
     * Websocket callbacks
     */
    socket.onopen = function(){
        console.log("Socket Connected");
        if(flag == 0){
            /*  Requesting game list  */
            flag = 1;
            listAllGames = JSON.stringify({type: 'lobby', info:'listGames'});
            console.log('request games from lobby');
            socket.send(listAllGames);
        } else if(flag == 1){
            /*  Updates game list  */
            $('.tableclass').remove();
            var json = JSON.stringify({type: 'lobby', info:'listGames'});
            console.log('request games from lobby');
            socket.send(json);
        } else if(flag == 2){
            console.log("Data already sent");
        }
    };

    socket.onclose = function(){
        console.log('Socket Disconnected');
    };

    socket.onerror = function(){
        console.log('Socket Error');
    };

    /*  When message received  */
    socket.onmessage = function(e){
        console.log('Got something');
        console.log(e.data);
        var obj = JSON.parse(e.data);
        /* chat message  */
        if (obj.type == 'chat') {
            $('#flow').text($('#flow').text() + obj.data + "\n");
        }
        /* Message sent from lobby */
        else if(obj.type == 'lobby') {
            /* If info == gameList the list of all available games is created */
            if(obj.info == 'gameList') {
                $.map(obj.games, function(e,i) {
                    $('#gameDiv').append('<tr class="tableclass"><td><button id='
                                         +e.name+
                                         ' class="gameButton" onClick="reply_click_gameLobby(this.id)">'
                                         +e.name+
                                         '</button></td><td>'
                                         +e.players+
                                         '</td></tr>');
                });
                /* Request to enter gameLobby accepted */
            } else if(obj.info == 'gameLobbyAccept'){
                exitFlag = 0;
                flag = 2;
                var requestGameBoards =JSON.stringify({type: 'gameLobby', time: new Date().getTime(), info:'gameBoards', game:obj.game});
                var requestGameRules =JSON.stringify({type: 'gameLobby', time: new Date().getTime(), info:'gameRules', game:obj.game});
                $('.tableclass').remove();
                $('#gameDiv').hide();
                socket.send(requestGameBoards);
                socket.send(requestGameRules);
                /* Unable to enter gameLobby  */
            } else if(obj.info == 'gameLobbyError'){
                alert('unable to join gamelobby, try again');
            }
            /* Request to leave game accepted  */
        } else if(obj.type == 'game'){
            if(obj.info == 'gameMsg'){
                startGame.onmessage(obj.gameData);
            } else if (obj.info == 'kickToGameLobby'){
                $('#quitDiv').remove();
                $('#appendHere').remove();
                $('.chat').hide();
                console.log('Send data');
                socket.send(requestGameLobby);
            }
            /* Message from gameLobby  */
        } else if(obj.type == 'gameLobby'){
            /* If info == gameBoards the list of all availabe game boards will be created */
            if(obj.info == 'gameBoards'){
                $('#information').hide();
                $('#boardDiv').show();
                $('#boardDivExit').show();
                $.map(obj.boards, function(e,i) {
                    $('#boardDiv').append('<tr class ="boardButton"><td><button value ='
                                          +obj.game+
                                          ' id='
                                          +e.id+
                                          ' class="boardButton" onClick="reply_click_gameBoard(this.id, this.value)">'
                                          +e.id+
                                          '</button></td><td>'
                                          +e.players+
                                          '</td><td>'
                                          +bool_to_string(e.started)+
                                          '</td></tr>');
                });
                /* Create button for creating new game */
                $('#boardDivExit').append('<button value ='
                                          +obj.game+
                                          ' id=-1 class="boardButton" onClick="reply_click_gameBoard(this.id, this.value)">Create new game</button>');
                /* Create button for leaving gameLobby */
                $('#boardDivExit').append('<button value = "'
                                          +obj.game+
                                          '" class="boardButton" onClick="reply_click_back_to_menu()">Back to menu</button>');
                /* if info == gameRules the rules for the game will be written to rules */
            } else if(obj.info == 'gameRules'){
                /* Show game rules */
                $('#information').hide();
                $('.rulesClass').show();
                $('#rules').val(obj.rules);

                /* Request to join board  */
            } else if(obj.info == "joinGame"){
                if(obj.board > 0){
                    /*Ok to join game */
                    $('.boardButton').remove();
                    $('.chat').show();
                    $('.rulesClass').hide();
                    $('#boardDiv').hide();
                    $('#boardDivExit').hide();
                    $('#waitingForPlayers').show();
                    $('#exitGame').append('<div id="quitDiv"></div>');
                    $('#overhead').append('<div id="appendHere"></div>');
                    $('#quitDiv').append('<button id="quitGame "value='
                                         +obj.game+
                                         ' onClick="reply_click_exit_game(this.value)">Exit</button> ');
                } else {
                    /* Denied to join game */
                    alert('Unable to join game, please try another board');
                }
                /* Accepted to start game */
            } else if(obj.info == "gameStart"){
                exitFlag = 1;
                $('#waitingForPlayers').hide();
                startGame = GameFactory.create(obj.game);
                startGame.enter(new GameSocket(socket, obj.game));
                /* Request to leave gameLobby  */
            } else if(obj.info == "kickToLobby"){
                flag = 0;
                $('#rules').val('');
                $('.rulesClass').hide();
                $('.boardButton').remove();
                $('#boardDiv').hide();
                $('#boardDivExit').hide();
                $('#gameDiv').show();
                $('#information').show();
                console.log('Send data');
                socket.send(listAllGames);
            } else if (obj.info == 'leaveGameAccept'){
                $('#quitDiv').remove();
                $('#appendHere').remove();
                $('.chat').hide();
                console.log('Send data');
                socket.send(requestGameLobby);
            }
        }
    }

    /*
     *  Once send is clicked the text written in
     *  mgs is passed as a Json-Object with type chat
     *  to the server. msg is then set to ''.
     */
    $('#send').click(function(){
        var txt = $('#msg').val();
        var json = JSON.stringify({type: 'chat', data: txt});
        console.log('Send data');
        socket.send(json);
        console.log(json);
        $('#msg').val('');
    });

    /*
     *  Once gameDiv is clicked, Json-object
     *  requestGamesLobby is sent to server.
     */
    $('#gameDiv').click(function(){
        console.log('Send data');
        socket.send(requestGameLobby);
        console.log(requestGameLobby);
    });

    /*
     *  Once exitGame is clicked, Json-object
     *  exitJson is sent to server.
     */
    $('#exitGame').click(function(){
        console.log('Send data');
        socket.send(exitJson);
    });

    /*
     *  Once boardDivExit is clicked, Json-object
     *  requestGame is sent to server if boardFlag == 0, else
     *  exitJson is sent.
     */
    $('#boardDivExit').click(function(){
        if(boardFlag == 0){
            console.log('Send data');
            socket.send(requestGame);
            console.log('Game requested');
        } else {
            socket.send(exitJson);
            console.log(exitJson);
        }
    });

    /*
     *  Once boardDiv is clicked, Json-object
     *  requestGame is sent to server.
     */
    $('#boardDiv').click(function(){
        console.log('Send data');
        socket.send(requestGame);
        console.log('Game requested');
    });
});
